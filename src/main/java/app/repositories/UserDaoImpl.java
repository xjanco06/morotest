package app.repositories;

import app.configuration.database.HibernateUtil;
import app.configuration.database.entity.User;
import app.helpers.errors.AnError;
import app.helpers.errors.DBPersistenceException;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import java.util.Collection;

import org.hibernate.Session;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User getUserById(Integer userId) {
        return getUser(userId);
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from User where username = :username", User.class)
                    .setParameter("username", username).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.NO_DATA,
                    "User " + username + " does not exist."), e);
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                    "Connection to database was unsuccessful. Transaction rollback called."), e);
        }
    }

    @Override
    public User updateUser(Integer userId, User user) {
        User oldUser = getUser(userId);

        if (oldUser != null) {
            oldUser.setName(user.getName());
            oldUser.setUsername(user.getUsername());
            oldUser.setPassword(user.getPassword());
            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();
                session.update(oldUser);
                transaction.commit();
            } catch (PersistenceException e) {
                LOGGER.error(e.getLocalizedMessage());
                throw new DBPersistenceException(new AnError(AnError.ErrorType.DATA_INCONSISTENCY,
                        "User with username '" + user.getUsername() + "' already exists."), e);
            } catch (Exception e) {
                LOGGER.error(e.getLocalizedMessage());
                throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                        "Connection to database was unsuccessful. Transaction rollback called."), e);
            } finally {
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
            }

            return getUserByUsername(oldUser.getUsername());
        }
        return null;
    }

    @Override
    public void deleteUser(Integer userId) {
        User oldUser = getUser(userId);
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.delete(oldUser);
            transaction.commit();
        } catch (NoResultException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.NO_DATA,
                    "User '" + userId + "' not found in database. Operation not completed."), e);
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                    "Connection to database was unsuccessful. Transaction rollback called."), e);
        } finally {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    @Override
    public Collection<User> getAllUsers() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from User", User.class).list();
        } catch (NoResultException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.NO_DATA,
                    "No users returned. Database is empty."), e);
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                    "Connection to database was unsuccessful. Transaction rollback called."), e);
        }
    }

    @Override
    public User createUser(User user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        } catch (NoResultException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.NO_DATA,
                    "User created but presence database check failed."), e);
        } catch (PersistenceException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.DATA_INCONSISTENCY,
                   "User with username '" + user.getUsername() + "' already exists."), e);
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                    "Connection to database was unsuccessful. Transaction rollback called."), e);
        } finally {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }

        return getUserByUsername(user.getUsername());
    }


    private User getUser(Integer id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from User where id = :id", User.class)
                    .setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.NO_DATA,
                    "User '" + id + "' not found."), e);
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            throw new DBPersistenceException(new AnError(AnError.ErrorType.CONNECTION_ERROR,
                    "Connection to database was unsuccessful. Transaction rollback called."), e);
        }
    }
}
