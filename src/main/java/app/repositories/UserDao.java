package app.repositories;

import app.configuration.database.entity.User;

import java.util.Collection;

/**
 * Repository retrieving User information from database
 * @author Marketa Jancova
 * @version 1.0
 */
public interface UserDao {
    User getUserByUsername(String username);
    User getUserById(Integer userId);
    User updateUser(Integer userId, User user);
    void deleteUser(Integer userId);
    User createUser(User user);
    Collection<User> getAllUsers();
}
