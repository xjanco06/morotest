package app.configuration.security;

import app.services.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Configuration of Spring Security
 * Endpoints starting with '/secured/users' require authentication
 * Endpoints starting with '/users' don't require authentication
 * @author Marketa Jancova
 * @version 1.0
 */
@EnableWebSecurity
public class SecurityConfiguration {
    @Configuration
    public static class DatabaseAuthenticationSecurity extends WebSecurityConfigurerAdapter {
        @Autowired
        private UserManager userManager;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable().httpBasic()
                    .and().authorizeRequests().antMatchers("/secured/users/**").authenticated()
                    .and().authorizeRequests().antMatchers("/users/**").permitAll();
        }

        @Bean
        public BCryptPasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }

        @Bean
        public DaoAuthenticationProvider authenticationProvider() {
            DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
            auth.setUserDetailsService(userManager);
            auth.setPasswordEncoder(passwordEncoder());
            return auth;
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) {
            auth.authenticationProvider(authenticationProvider());
        }
    }
}

