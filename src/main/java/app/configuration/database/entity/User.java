package app.configuration.database.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Entity represents database table
 * Related DB table and sequence incrementing primary key should be created in database on startup
 * @author Marketa Jancova
 * @version 1.0
 */
@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "user-sequence")
    @GenericGenerator(
            name = "user-sequence",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    private Integer id;

    private String name;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    protected User() {}

    public User(Integer id, String name, String username, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return username + ":" + password;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  User) {
            User compareTo = (User)obj;
            return (id == null && compareTo.id == null)
                    || (id != null && id.equals(compareTo.id));
        }
        return false;
    }
}
