package app.configuration.database;

import app.configuration.database.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Configuration of connection to database
 * In this case I use local PostgreSQL database
 * @author Marketa Jancova
 * @version 1.0
 */
public class HibernateUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateUtil.class);

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration sessionConfiguration = new Configuration();
                Properties sessionProperties = new Properties();
                sessionProperties.put(Environment.DRIVER, "org.postgresql.Driver");
                sessionProperties.put(Environment.URL, "jdbc:postgresql://localhost:5432/postgres");
                sessionProperties.put(Environment.USER, "postgres");
                sessionProperties.put(Environment.PASS, "admin");
                sessionProperties.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
                sessionProperties.put(Environment.SHOW_SQL, "true");
                sessionProperties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                sessionProperties.put(Environment.HBM2DDL_AUTO, "update");
                sessionProperties.put(Environment.NON_CONTEXTUAL_LOB_CREATION, true);

                sessionConfiguration.setProperties(sessionProperties);
                sessionConfiguration.addAnnotatedClass(User.class);

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(sessionConfiguration.getProperties()).build();
                sessionFactory = sessionConfiguration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                LOGGER.error("Error while doing database session configuration. " + e.getLocalizedMessage());
            }
        }
        return sessionFactory;
    }
}

