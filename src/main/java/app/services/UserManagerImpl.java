package app.services;

import app.helpers.errors.DBPersistenceException;
import app.services.model.UserDto;
import app.configuration.database.entity.User;
import app.helpers.errors.AnError;
import app.repositories.UserDao;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserManagerImpl implements UserManager{
    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.getUserByUsername(username);
        System.out.flush();
        if (user != null) {
            return new org.springframework.security.core.userdetails.User(user.getUsername(),
                    user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList("USER"));
        } else {
            throw new UsernameNotFoundException("User '" + username + "' does not exist.");
        }
    }

    @Override
    public Either<AnError, UserDto> getUserById(Integer userId) {
        try {
            User user = userDao.getUserById(userId);
            return Either.right(new UserDto(user.getId(), user.getName(), user.getUsername(), user.getPassword()));
        } catch (DBPersistenceException e) {
            return Either.left(e.getErrorThrown());
        }
    }

    @Override
    public Either<AnError, Collection<UserDto>> getAllUsers() {
        try {
            Collection<User> users = userDao.getAllUsers();
            return Either.right(users.stream().map(u -> new UserDto(u.getId(), u.getName(), u.getUsername(), u.getPassword())).collect(Collectors.toList()));
        } catch (DBPersistenceException e) {
            return Either.left(e.getErrorThrown());
        }
    }

    @Override
    public Option<AnError> deleteUser(Integer id) {
        try {
            userDao.deleteUser(id);
        } catch (DBPersistenceException e) {
            return Option.some(e.getErrorThrown());
        }
        return Option.none();
    }

    @Override
    public Either<AnError, UserDto> updateUser(Integer id, String name, String username, String password) {
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        User userMeta = new User(id, name, username, encodedPassword);
        try {
            User updatedUser = userDao.updateUser(id, userMeta);
            return Either.right(new UserDto(updatedUser.getId(), updatedUser.getName(), updatedUser.getUsername(), updatedUser.getPassword()));
        } catch (DBPersistenceException e) {
            return Either.left(e.getErrorThrown());
        }
    }

    @Override
    public Either<AnError, UserDto> createUser(String name, String username, String password) {
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        User userMeta = new User(null, name, username, encodedPassword);
        try {
            User newUser = userDao.createUser(userMeta);
            return Either.right(new UserDto(newUser.getId(), newUser.getName(), newUser.getUsername(), newUser.getPassword()));
        } catch (DBPersistenceException e) {
            return Either.left(e.getErrorThrown());
        }
    }
}