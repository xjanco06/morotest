package app.services;

import app.services.model.UserDto;
import app.helpers.errors.AnError;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collection;

/**
 * Service translating database entities to fancy objects
 * @author Marketa Jancova
 * @version 1.0
 */
public interface UserManager extends UserDetailsService {
    Either<AnError, UserDto> getUserById(Integer id);
    Either<AnError, Collection<UserDto>>  getAllUsers();
    Option<AnError> deleteUser(Integer id);
    Either<AnError, UserDto> updateUser(Integer id, String name, String username, String password);
    Either<AnError, UserDto> createUser(String name, String username, String password);
}
