package app.services.model;

import org.springframework.lang.Nullable;

public class UserDto {
    private Integer id;
    private String name;
    private String username;
    private String password;

    public UserDto(@Nullable Integer id, @Nullable String name, @Nullable String username, @Nullable String password) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.username = username;
    }

    public void setId(Integer id) { this.id = id; }

    public Integer getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    public String getPassword() { return password; }

    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name + ", username=" + username + "]";
    }
}
