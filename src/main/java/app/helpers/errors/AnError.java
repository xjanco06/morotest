package app.helpers.errors;

import javax.ws.rs.core.Response;

/**
 * Helper object representing application error.
 * @author Marketa Jancova
 * @version 1.0
 */
public class AnError {
    private ErrorType errorType;
    private String errorMessage;

    public AnError(ErrorType errorType, String errorMessage) {
        this.errorMessage = errorMessage;
        this.errorType = errorType;
    }

    public enum ErrorType {
        VALIDATION_ERROR("Validation error", Response.Status.BAD_REQUEST),
        NO_DATA("Data not present", Response.Status.OK),
        CONNECTION_ERROR("Connection error", Response.Status.SERVICE_UNAVAILABLE),
        DATA_INCONSISTENCY("Data inconsistency", Response.Status.CONFLICT),
        INTERNAL_EXCEPTION("Internal exception", Response.Status.INTERNAL_SERVER_ERROR);

        private String errorType;
        private Response.Status status;

        ErrorType(String errorType, Response.Status status) {
            this.errorType = errorType;
            this.status = status;
        }

        public String getErrorType() {
            return errorType;
        }

        public void setErrorType(String errorType) {
            this.errorType = errorType;
        }

        public Response.Status getStatus() {
            return status;
        }

        public void setStatus(Response.Status status) {
            this.status = status;
        }
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "Request could not be completed. Result: '" + errorType.status.getStatusCode() + ": "
                + errorType.getErrorType() + "'. Description: " + errorMessage;
    }
}
