package app.helpers.errors;

/**
 * Wrapping Exception for Exceptions thrown in database ot during database access
 * @author Marketa Jancova
 * @version 1.0
 */
public class DBPersistenceException extends RuntimeException {
    private AnError errorThrown;

    public DBPersistenceException(AnError errorThrown, Throwable err) {
        super(err);
        this.errorThrown = errorThrown;
    }

    public void setErrorThrown(AnError errorThrown) {
        this.errorThrown = errorThrown;
    }

    public AnError getErrorThrown() {
        return errorThrown;
    }
}
