package app.api.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserForm {
    @NotNull
    @Pattern(regexp = "^[(a-zA-Z)|\\s]*$", message = "Name is not in correct format.")
    @Size(min = 3, max = 30, message = "Name length has to be between 3 and 30 characters.")
    private String name;

    @NotNull
    @Pattern(regexp = "^\\w*$", message = "Username is not in correct format.")
    @Size(min = 3, max = 10, message = "Username length has to be between 3 and 10 characters.")
    private String username;

    @NotNull
    @Size(min = 5, max = 30, message = "Password length has to be between 5 and 30 characters.")
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User Form [name=" + name + ", username=" + username + ", password: " + password + "]";
    }
}
