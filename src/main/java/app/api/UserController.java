package app.api;

import app.api.form.UserForm;
import app.configuration.database.entity.User;
import app.services.model.UserDto;
import app.helpers.errors.AnError;
import com.google.gson.Gson;
import io.atlassian.fugue.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import app.services.UserManager;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

/**
 * Rest Controller providing endpoints for retrieving data about users
 * Endpoints do not require authentication
 * @author Marketa Jancova
 * @version 1.0
 */
@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    UserManager userManager;

    @GetMapping(value = "/{userId}", produces = "application/json")
    public Response getUserById(@PathVariable Integer userId) {
        Either user = userManager.getUserById(userId);
        return parseResponseFromResults(user);
    }

    @GetMapping(value = "/all", produces = "application/json")
    public Response getAllUsers() {
        Either users = userManager.getAllUsers();
        return parseResponseFromResults(users);
    }

    @PostMapping(value = "/createUser", consumes = "application/json", produces = "application/json")
    public Response createNewUser(@Valid @RequestBody UserForm userForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new Gson().toJson(
                    bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()))).build();
        }

        Either user = userManager.createUser(userForm.getName(), userForm.getUsername(), userForm.getPassword());
        return parseResponseFromResults(user);
    }

    @PatchMapping(value = "/{userId}/modifyUser", consumes = "application/json", produces = "application/json")
    public Response modifyUser(@Valid @RequestBody UserForm userForm, @PathVariable Integer userId, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new Gson().toJson(
                    bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()))).build();
        }

        Either user = userManager.updateUser(userId, userForm.getName(), userForm.getUsername(), userForm.getPassword());
        return parseResponseFromResults(user);
    }

    private Response parseResponseFromResults(Either<AnError, Object> result) {
        if (result.isRight()) {
            return Response.ok().entity(new Gson().toJson(result.right().get())).build();
        } else {
            return Response.status(result.left().get().getErrorType().getStatus())
                    .entity(new Gson().toJson(result.left().get().getErrorMessage())).build();
        }
    }
}
