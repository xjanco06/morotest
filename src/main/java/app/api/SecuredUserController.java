package app.api;

import app.helpers.errors.AnError;
import io.atlassian.fugue.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import app.services.UserManager;

import javax.ws.rs.core.Response;

/**
 * Rest Controller providing endpoint that deletes user
 * Endpoint requires authentication
 * @author Marketa Jancova
 * @version 1.0
 */
@RestController
@RequestMapping("secured/users")
public class SecuredUserController {
    @Autowired
    UserManager userManager;

    @DeleteMapping(value = "/{userId}/deleteUser", produces = "application/json")
    public Response deleteUser(@PathVariable Integer userId) {
        Option<AnError> deleteSuc = userManager.deleteUser(userId);
        if (deleteSuc.isEmpty()) {
            return Response.ok().build();
        } else {
            return Response.status(deleteSuc.get().getErrorType().getStatus())
                    .entity(deleteSuc.get().getErrorMessage()).build();
        }
    }
}
