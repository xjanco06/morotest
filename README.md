# MoroTest: Endpoints  

## [GET] /users/{userId}
#### Request params:  
 - Path: userId (identification of user in database)  
#### Request example:
      curl -X GET localhost:8090/users/1
#### Response:  
 - JSON object representing User

  
## [GET] /users/all
#### Request params: None
#### Request example:
      curl -X GET localhost:8090/users/all 
#### Response:  
 - (Code 200) JSON array representing list of Users  

## [POST] /users/createUser
#### Request params:  
 - Query: userForm (JSON object containing attributes name, username, password)
#### Request example:
      curl -v -X POST localhost:8090/users/createUser -H "Content-type:application/json"  
	  -d "{\"name\":\"Ingmar Bergmann\",\"username\":\"Iggy\",\"password\":\"myCat\"}" 
#### Response:  
 - (Code 201) JSON object representing new User
 
 
## [PATCH] /users/{userId}/modifyUser
#### Request params:  
 - Path: userId (identification of user in database)  
 - Query: userForm (JSON object containing attributes name, username, password)
#### Request example:
      curl -v -X PATCH localhost:8090/users/1/modifyUser -H "Content-type:application/json"  
	  -d "{\"name\":\"Ingmar Bergmann\",\"username\":\"Iggy\",\"password\":\"myCat\"}" 
#### Response:
 - (Code 200) JSON object representing updated User

## [DELETE] /secured/users/{userId}/deleteUser
#### Request params:  
 - Path: userId (identification of user in database)
#### Request example:
	curl --user Iggy:myCat -v -X DELETE localhost:8090/secured/users/1/deleteUser  
Note: expects user with these credentials present in database  
#### Response:
 - (Code 200) Empty when success